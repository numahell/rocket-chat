# Présentation outil Rocket.Chat

Tutoriels d'utilisation et d'administration de Rocket.Chat pour Alternatiba. Visibles sur [alternatiba.frama.io/rocket-chat](https://alternatiba.frama.io/rocket-chat/)

## Contribuer

Les tutoriels sont écrits en [markdown](https://daringfireball.net/projects/markdown/syntax),
 et sont tranformés en html par [reveal-md]() sous forme de slide au format [RevealJS](https://revealjs.com/).

Le fichiers source se trouvent dans le dossier [src/](src/), et les images dans le dossier [src/media/](src/media/).

Pour séparer les slides, utiliser `---` pour un groupe de slides verticaux et `----` entre chaque slide.

Les messages de commit sont en français pour ce projet.

## Prérequis

Installer [node v14](https://pandoc.org/)

## Générer le diaporama

Clone le dépôt

```
git clone git@framagit.org:alternatiba/rocket-chat.git
```

Générer les présentations en mode "watch"

```
npm start
```

Déployer automatiquement les modifications

```
git push
```

## Publier les slides sur alternatiba.frama.io/rocket-chat

Publier le dossier public sur le dépôt : pour cela, créer une merge request sur la branche master.
